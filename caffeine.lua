-- local caffeine = hs.menubar.new()
-- function setCaffeineDisplay(state)
--     if state then
--         caffeine:setTitle("☕️")
--     else
--         caffeine:setTitle("💤")
--     end
-- end
--
-- function caffeineClicked()
--     setCaffeineDisplay(hs.caffeinate.toggle("displayIdle"))
-- end
--
-- if caffeine then
--     caffeine:setClickCallback(caffeineClicked)
-- 	hs.caffeinate.set("displayIdle", true)
--     setCaffeineDisplay(hs.caffeinate.get("displayIdle"))
-- end

function blackScreen()
	-- hs.osascript.applescript('tell application "Finder"\nsleep\nend tell')
	hs.task.new('/usr/bin/pmset',nil,{'displaysleepnow'}):start()
	-- hs.task.new('/Volumes/UserData/AV Team/screenoff',nil):start()
end

local screenoff = hs.menubar.new()
screenoff:setTitle('💤')
screenoff:setTooltip('Put display to sleep.')
screenoff:setClickCallback(blackScreen)

-- HOTKEY SLEEP DISPLAYS
hs.hotkey.bind({'cmd','ctrl','alt'},'s',blackScreen)
