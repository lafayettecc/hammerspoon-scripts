-- HAMMERSPOON CONFIGURATION
require "helpers"
require "keynote"
dofile "autoReload.lua"

-- hs.hotkey.bind({'cmd','ctrl','alt','shift'},'s',function()
-- 	print ("attempting to sleep")
-- 	hs.task.new('/usr/bin/pmset',nil,{'sleepnow'}):start()
-- end)

-- OPTIONAL INCLUDES
dofile "mouseHighlight.lua"
dofile "spotify.lua"
dofile "keynote2vmix.lua"
dofile "live_event.lua"
dofile "window_moves.lua"
dofile "caffeine.lua"
dofile "fix-pro6.lua"
-- dofile "scrolling.lua"

-- notify user that configuration file has been loaded fully
notify("Hammerspoon config loaded")