function farmer_helper()
	alert('starting farmer in 5 seconds')
	hs.timer.doAfter(5, farmer)
end
function farmer()
	local mousepos = hs.mouse.getAbsolutePosition()
	-- fertilizing potatoes or carrots takes 2.4 pieces of bonemeal on average
	local farmer_delay = 50
	
	for i=1,27 do
		-- planting with '5'
		-- hs.eventtap.keyStroke({},'5')
		-- hs.eventtap.rightClick(mousepos)
		send({},'5',farmer_delay)
		click('right', mousepos, farmer_delay)

		-- fertilizing with '6'
		-- hs.eventtap.keyStroke({},'6')
		send({},'6',farmer_delay)
		for j=1,3 do
			-- hs.eventtap.rightClick(mousepos)
			click('right',mousepos,farmer_delay)
		end

		-- harvesting
		-- hs.eventtap.leftClick(mousepos)
		click('left',mousepos,farmer_delay)
	end
	-- planting with '5'
	-- hs.eventtap.keyStroke({},'5')
	-- hs.eventtap.rightClick(mousepos)
	send({},'5',farmer_delay)
	click('right',mousepos,farmer_delay)
end
hs.hotkey.bind({"cmd","alt","ctrl"}, "9", farmer_helper)

