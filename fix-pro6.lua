-- CONFIGURE PROPRESENTER SETTINGS
local pro6_bundle_id = 'com.renewedvision.ProPresenter6'
local pro6_icon_path = "/Volumes/UserData/AV Team/Icons/Pro6_16.png"

-- CONFIGURE DISPLAY SETTINGS
-- hammerspoon reads all the displays as one giant canvas of pixels
-- if two displays are ordered horizontally:
-- the main display will have these coordinates:
-- {x=0,y=0,w=display1_width,h=display1_height}
-- 
-- a secondary display on the left will have these coordinates
-- {x=-display2_width,y=0,w=display2_width,h=display2_height}
--
-- a secondary display on the right will have these coordinates
-- {x=display1_width,y=0,w=display2_width,h=display2_height}
--
-- three 1920x1080 displays arranged vertically look like this
-- {x=0,y=0,w=1920,h=1080}
-- {x=0,y=1080,w=1920,h=1080}
-- {x=0,y=2160,w=1920,h=1080}
local main_display_dimensions = {x=0,y=0,w=1920,h=1080}
local stage_display_dimensions = {x=0,y=1080,w=1920,h=1080}
local output_display_dimensions = {x=0,y=2160,w=1920,h=1080}


-- STOP EDITING
local swapped = False
function fix_pro6_windows()
	--get pro6 app (must be running for this to work)
	local pro6 = hs.application.find(pro6_bundle_id)
		
	-- there will be a number of four windows
	-- Main ProPresenter Window Title:   'Registered to: LAFAYETTE COMMUNITY CHURCH'
	-- Hidden ProPresenter Window Title: 'Updating ProPresenter 6'
	-- Output and Stage Display Titles: '' (the empty string)
	local windows = pro6:allWindows()
	
	-- Check windows to find stage and output displays
	-- We guess that the first pro6 window without a title
	-- is the stage display.
	local stage  = nil
	local output = nil
	
	for i=1, #windows do
		if windows[i]:title() == '' then
			if not stage then
				stage = windows[i]
			else
				output = windows[i]
			end
		end
	end
	
	-- the following code makes sure that the windows get
	-- swapped with each other every time this function is called
	if swapped then
	    stage, output = output, stage
	end
	swapped = not swapped
    
    -- this actually moves the windows around on the desktop canvas		
	stage:setFrame(stage_display_dimensions,0)
	output:setFrame(output_display_dimensions,0)
	
	notify('ProPresenter windows were arranged')
end

local pro6_button = hs.menubar.new()
pro6_button:setIcon(pro6_icon_path, false)
pro6_button:setTooltip("Fix ProPresenter Windows")
pro6_button:setClickCallback(fix_pro6_windows)