function keynoteIsPlaying()
	local script = [[
	if application "Keynote" is running then
		tell application "Keynote"
			if playing is true then
				return 1
			else
				return 0
			end if
		end tell
	else
		return -1
	end if
	]]
	local success, result, raw = hs.osascript.applescript(script)
	if success then
		if result == 1 then
			return true
		else
			return false
		end
	end
end

function keynotePlayingCurrentSlide()
	local script = [[
	global slideNumber
	set slideNumber to -1
	if application "Keynote" is running then
		tell application "Keynote"
			if playing is true then
				set slideNumber to get slide number of current slide of front document
			end if
		end tell
	end if
	return slideNumber
	]]
	local success, result, raw = hs.osascript.applescript(script)
	if success then
		if result == -1 then
			return false
		else
			return result
		end
	end
end
	

function keynoteSlideNotes()
	local script = [[
	global slideNotes
	set slideNotes to ""
	if application "Keynote" is running then
		tell application "Keynote"
			set slideNotes to get presenter notes of current slide of front document
			return slideNotes
		end tell
	else
		return -1
	end if
	]]
	local success, result, raw = hs.osascript.applescript(script)
	if success then
		if result == -1 then
			return false
		else
			return result
		end
	end
end

function keynotePlayingSlideNotes()
	local script = [[
	global slideNotes
	set slideNotes to ""
	if application "Keynote" is running then
		tell application "Keynote"
			if playing is true then
				set slideNotes to get presenter notes of current slide of front document
			end if
			return slideNotes
		end tell
	else
		return -1
	end if
	]]
	
	local success, result, raw = hs.osascript.applescript(script)
	if success then
		if result == -1 then
			return false
		else
			return result
		end
	end
end
