-- SPOTIFY ITEMS
hs.hotkey.bind({"cmd", "alt", "ctrl"}, "0", function()
	hs.spotify.displayCurrentTrack()
end)

local fader
local spotify_fadeout_timer


-- seconds: plan to play spotify for this many seconds
function prepare_for_gathering(seconds, forced)
	
	-- if a spotify auto fade_out timer is running, stop it
	-- so we don't get weirdness
	if spotify_fadeout_timer and spotify_fadeout_timer:running() then
		spotify_fadeout_timer:stop()
	end
	
	
	-- to avoid making a jarring start and stop
	-- wait until this song is over before setting up new list
	if not forced then
		-- when will this song end
		local song_duration = hs.spotify.getDuration()
		local song_position = hs.spotify.getPosition()
		local song_time_remaining = song_duration - song_position
		if song_time_remaining > 1 and song_duration < seconds then
			print('Spotify Song has ' .. song_time_remaining .. ' seconds left. I will try again then.' )
			local try_again = song_time_remaining
			hs.timer.doAfter(try_again, function() prepare_for_gathering(seconds - try_again, true) end)
			return
		end
	end
	
	
	notify('Generating Spotify playlist for ' .. seconds .. ' seconds.')
	hs.spotify.pause()

	-- this script works best if shuffling is off
	-- and repeating is on, so force those settings
	local script = [[
	tell application "Spotify"
		set shuffling to false
		set repeating to true
	end tell
	]]
	hs.osascript.applescript(script)
	
	
	total = 0
	songcount = 0
	songtitles = {}
	notify_string = 'Current Playlist'

	-- walk through playlist to determine length of next songs
	while 1 do
		songcount = songcount + 1
		local duration = hs.spotify.getDuration()
		local title = hs.spotify.getCurrentTrack()
		table.insert(songtitles, title)
		print(title .. ' ' .. duration)
		total = total + duration
		print (total)
		if total >= seconds then
			print ('reached the limit: breaking out')
			print ('Current Playlist:')
			for i,title in ipairs(songtitles) do
				print (title)
				notify_string = notify_string .. '\n' .. title
			end
			break
		end
		-- builtin spotify commands return before command is complete
		hs.spotify.next()
		hs.timer.usleep(250000)
	end
	
	-- go back to the song we started on
	for i=songcount,2,-1 do hs.spotify.previous() end
	target_duration = seconds
	
	-- back this song up
	print ('Starting ' .. songtitles[1] .. ' at ' .. (total - target_duration))
	hs.spotify.setPosition(total - target_duration)
	
	-- start playing the generated playlist
	spotify_fade_in(5)
	
	-- automatically fade out music when worship gathering is scheduled to start
	spotify_fadeout_timer = hs.timer.doAfter(seconds-5, function() spotify_fade_out(5, true) end)
	hs.alert.show('Spotify Automatic Playlist:\n===============================\n' .. notify_string .. '\n\nAUTO FADE IN '..seconds.. ' seconds.',5)
	
	-- hs.timer.doAfter(seconds, function() spotify_prepare() end)
	return total
end

function old_prepare_for_gathering()
	-- these items run asynchronously, so we have to set timer intervals
	notify('resetting Spotify track to have 3 minutes left')
	
	-- first, trigger the fade_out timer
	spotify_fade_out(2)
	
	-- schedule the reset and fade-in
	hs.timer.doAfter(2.1, function()
		reset_to_time_left(3*60 + 5)
		spotify_fade_in(1,100)
	end)
end

function get_track_duration()
	-- applescript code because hammerspoon can't return
	-- track time yet
	local script = [[
	tell application "Spotify"
		delay .1
		set tt to duration of current track
		set tt to tt / 1000
		return tt
	end tell
	]]
	
	local success, result, raw = hs.osascript.applescript(script)
	if success then
		return result
	end
end

function reset_to_time_left(n)
	-- resets the song to n seconds left
	local track_time = get_track_duration()
	local new_time = track_time - n
	
	-- send player position to spotify
	if (new_time > 0) then hs.spotify.setPosition(new_time) end
end

function spotify_fade_out(duration, advance)
	-- cancel the spotify fader timer if it is running
	if fader and fader:running() then fader:stop() end
	
	local duration = duration or 2
	local volinc = 2
	local steps = hs.spotify.getVolume() / volinc
	local time_interval = duration / steps
	
	-- this is asynchronous
	fader = hs.timer.doUntil(
		function()
			if hs.spotify.getVolume() == 0 then
				hs.spotify.pause()
				if advance then
					hs.spotify.next()
				end
				spotify_prepare()
				return true
			end
		end,
		function(t)
			local vol = hs.spotify.getVolume()
			if vol == 0 then
				t:stop()
			else
				hs.spotify.setVolume(vol - volinc)
			end
		end,
		time_interval
	)
end

function spotify_fade_in(duration, targetVolume)
	-- cancel the spotify fader timer if it is running
	if fader and fader:running() then fader:stop() end
	
	local duration = duration or 2
	local targetVolume = targetVolume or 90
	-- print (targetVolume)
	
	local volinc = 2
	local steps = targetVolume / volinc
	local time_interval = duration / steps
	
	hs.spotify.setVolume(0)
	hs.spotify.play()
	
	-- this is asynchronous
	fader = hs.timer.doUntil(
		function()
			if hs.spotify.getVolume() >= targetVolume then
				-- print 'returning true'
				return true
			end
		end,
		function(t)
			local vol = hs.spotify.getVolume()
			if vol >= targetVolume then
				t:stop()
			else
				-- a bug in the spotify implementation treats an increment of n as (n-1)
				-- print ('changing volume to ' .. (vol + volinc + 1))
				hs.spotify.setVolume(vol + volinc + 1)
			end
		end,
		time_interval
	)	
end

function spotify_prepare()
	-- this gets Spotify ready to play as soon as the button is clicked
	hs.spotify.setVolume(100)
	hs.spotify.setPosition(5)
end

-- spotify scheduled items
local first_gathering_spotify_prepare = hs.timer.doAt('08:50', 24*60*60, function()
	if os.date("%A") == 'Sunday' then
		prepare_for_gathering(10 * 60 + 5, false)
	end
end)
first_gathering_spotify_prepare:start()

local second_gathering_spotify_prepare = hs.timer.doAt('10:35', 24*60*60, function()
	if os.date("%A") == 'Sunday' then
		prepare_for_gathering(10 * 60 + 5, false)
	end
end)
second_gathering_spotify_prepare:start()


-- spotify menu items
local spotify_auto_menu = hs.menubar.new()
spotify_auto_menu:setTitle('[AUTO PLAYLIST: ON]')
spotify_auto_menu:setClickCallback(function ()
	if first_gathering_spotify_prepare:running() then
		first_gathering_spotify_prepare:stop()
		second_gathering_spotify_prepare:stop()
		spotify_auto_menu:setTitle('[AUTO PLAYLIST: OFF]')
	else
		first_gathering_spotify_prepare:start()
		second_gathering_spotify_prepare:start()
		spotify_auto_menu:setTitle('[AUTO PLAYLIST: ON]')
	end		
end)

local spotify_fadeout = hs.menubar.new()
local spotify_fadein = hs.menubar.new()
spotify_fadeout:setClickCallback(function () spotify_fade_out(2) end)
-- spotify_fadeout:setIcon("/Volumes/UserData/AV Team/Icons/Spotify_32.png", false)
spotify_fadeout:setTitle('⤵︎  ')
spotify_fadein:setClickCallback(function () spotify_fade_in(1, 100) end)
spotify_fadein:setIcon("/Volumes/UserData/AV Team/Icons/Spotify_32.png", false)
spotify_fadein:setTitle('⤴︎   ')
