-- turn on menu bar item
local live_watch_menu = hs.menubar.new()
local timer1
local timer2
local eventID
local progress
local old_progress
local apitoken = 'svkcehTl4Rql3fqqjAZzinPPeXPEpFpHUdT0vzNZ'
local controlURI = 'https://lafayettecc.org:8999/?control=true&apitoken='..apitoken

function getKeynoteEvent()
	local script = [[
	global slideNotes
	global eventID
	global eventopentag
	global eventtaglength
	global eventclosetag

	set eventopentag to "event<"
	set eventtaglength to length of eventopentag
	set eventclosetag to ">"

	set opentag to "live["
	set taglength to length of opentag
	set closetag to "]"

	set eventID to ""

	on check_for_event()
		set leftCoord to offset of eventopentag in slideNotes
		set rightCoord to offset of eventclosetag in slideNotes
		if leftCoord is greater than 0 and rightCoord is greater than 0 then
			set eventID to get characters (leftCoord + eventtaglength) thru (rightCoord - 1) of slideNotes as string
		end if
	end check_for_event
	
	if application "Keynote" is running then
		tell application "Keynote"
			repeat with i from 1 to (count slides of first document)
				set slideNotes to get presenter notes of slide i of first document
				my check_for_event()
				if eventID is not equal to "" then
					return eventID
					exit repeat
				end if
			end repeat
		end tell
	else
		return 0
	end if
	]]
	
	local success, result, raw = hs.osascript.applescript(script)
	if success and result~=0 then
		eventID = result
		notify('Found Keynote Live Event: ' .. eventID)
		return result
	end
end

function getSlideNotes()
	local script = [[
	global slideNotes
	set slideNotes to ""
	if application "Keynote" is running then		
		tell application "Keynote"
			set slideNotes to get presenter notes of current slide of front document
	
			if slideNotes is not equal to "" then
				return slideNotes
			end if
		end tell
	else
		return -1
	end if
	]]
	
	local success, result, raw = hs.osascript.applescript(script)
	if success then
		if result == -1 then
			eventID = nil
			return false
		else
			return result
		end
	end
end

function keynoteLiveWatcher()
	if (not eventID) then return end
	local notes = getSlideNotes()
	if not notes then return end
	
	local progress_match = 'live[%d]'
	_, _, progress = string.find(notes, 'live%[(%d+)%]')
	if not progress or progress == old_progress then
		return
	else
		old_progress = progress
		updateEvent(progress)
		if progress == '999' then
			local secs = 1*60
			local msg = 'Progress of 999 was sent... setting a reset timer for ' .. secs .. ' seconds.'
			print (msg)
			notify(msg)
			hs.timer.doAfter(secs, function()
				updateEvent(0)
			end)
		end
	end
end

function updateEvent(progress)
	notify('updating live event ' .. eventID .. ' to ' .. progress)
	local url = controlURI .. '&event=' .. eventID .. '&progress=' .. progress
	print (url)
	hs.http.asyncGet(url, nil, function()end);
end

function live_watch_menuUpdate()
    if timer2:running() then
		live_watch_menu:setTitle("[" .. eventID .. "] LIVE!")
		return
	end
	if timer1:running() then
		live_watch_menu:setTitle("LIVE-WAITING")
    else
        live_watch_menu:setTitle("LIVE-OFF")
    end
end

function live_watch_menuClicked()
    if timer2:running() then
		timer2:stop()
		eventID = false
		live_watch_menuUpdate()
	else
		timer1:start()
		live_watch_menuUpdate()
	end
end


--  this timer watches for Keynote Event
timer1 = hs.timer.doEvery(5, function()
	getKeynoteEvent()
	
	if eventID then
		timer1:stop()
		timer2:start()
		live_watch_menuUpdate()
	end
end)
timer1:start()

timer2 = hs.timer.doEvery(1, function()
	keynoteLiveWatcher()
	if not eventID then
		timer2:stop()
		timer1:start()
		live_watch_menuUpdate()
	end
end)
timer2:stop()


if live_watch_menu then
    live_watch_menu:setClickCallback(live_watch_menuClicked)
    live_watch_menuUpdate()
end
