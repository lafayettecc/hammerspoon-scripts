-- HELPERS.LUA
function notify(s)
	hs.notify.new({title="Jeff's Hammerspoon", informativeText=s}):send()
end

function alert(s)
	hs.alert.show(s)
end

function delay(ms)
	secs = ms / 1000
	if secs > 5 then
		print 'ERROR: Delay cannot be greater than 5 seconds'
		return
	end
	local t0 = os.clock()
	while os.clock() - t0 <= secs do end
end

-- 'k' is a one character string
function send(mods, k, delay_time)
	local delay_time = delay_time or 100
	hs.eventtap.event.newKeyEvent(mods,k,true):post()
	delay(delay_time)
	hs.eventtap.event.newKeyEvent(mods,k,false):post()
end

function click(button, pos, delay_time)
	local delay_time = delay_time or  100
	local type = {
		down = hs.eventtap.event.types['leftMouseDown'],
		up = hs.eventtap.event.types['leftMouseUp']
	}
	if button == 'right' then
		type = {
			down = hs.eventtap.event.types['rightMouseDown'],
			up = hs.eventtap.event.types['rightMouseUp']
		}
	end
	
	hs.eventtap.event.newMouseEvent(type.down, pos):post()
	delay(delay_time)
	hs.eventtap.event.newMouseEvent(type.up, pos):post()
end