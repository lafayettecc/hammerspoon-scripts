-- check to see if the keynote slide has changed
-- if it has, tell vmix to switch to the computer input for a brief time.

-- time to show the computer slide on vmix before returning to previous input
local vmix_computer_timer
local vmix_computer_time = 30
local vmix_computer_input = 6
local vmix_default_input = 5
local vmix_endpoint = 'http://192.168.1.5:8088/api/?'
local last_slide = -1
local active = false

local keynoteChecker
local keynoteWatcher

keynoteChecker = hs.timer.new(10, function()
	-- print "checking if keynote is active"
	if keynoteIsPlaying() then
		print ('Keynote is Playing... starting watcher for vMix')
		keynoteChecker:stop()
		keynoteWatcher:start()
	end
end)

keynoteWatcher = hs.timer.new(1, function()
	local slideNotes = keynotePlayingSlideNotes()
	local curslide = keynotePlayingCurrentSlide()
	if slideNotes == false or curslide == -1 then
		print ('Keynote is Not Playing... ending watcher for vMix')
		keynoteWatcher:stop()
		keynoteChecker:start()
	else
		_, _, duration = string.find(slideNotes, 'vmix%[(%d+)%]')
		if duration and curslide ~= last_slide then
			duration = duration + 0
			vmixShowComputer(duration)
			last_slide = curslide
		end
	end
	
	
	-- local curslide = keynotePlayingCurrentSlide()
	-- if curslide == -1 then
	-- 	print ('Keynote is Not Playing... ending watcher for vMix')
	-- 	keynoteWatcher:stop()
	-- 	keynoteChecker:start()
	-- elseif curslide ~= last_slide then
	-- 	last_slide = curslide
	-- 	vmixShowComputer(vmix_computer_time)
	-- end
end)

keynoteChecker:start()

function vmixShowComputer(t)
	if active == false then return end
	local url
	-- show computer input
	url = vmix_endpoint..'Function=Fade&Duration=1000&Input='..vmix_computer_input
	print(url)
	hs.http.asyncGet(url, nil, function() end)
	
	-- fade back to camera input
	url = vmix_endpoint..'Function=Fade&Duration=1000'
	if vmix_computer_timer then
		vmix_computer_timer:stop()
	end
	vmix_computer_timer = hs.timer.doAfter(t, function() hs.http.asyncGet(url, nil, function() end) end)
end

-- menubar
local vmix_watch_menu = hs.menubar.new()

function vmix_watch_menuUpdate()
	if active then
		vmix_watch_menu:setTitle("[VMIX ON]  ")
	else
		vmix_watch_menu:setTitle("[VMIX OFF]  ")
	end
end

function vmix_watch_menuClicked()
	active = not active
	vmix_watch_menuUpdate()
end


if vmix_watch_menu then
    vmix_watch_menu:setClickCallback(vmix_watch_menuClicked)
    vmix_watch_menuUpdate()
end