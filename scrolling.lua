-- HANDLE SCROLLING
local oldmousepos = {}
local scrollmult = -4	-- negative multiplier makes mouse work like traditional scrollwheel

-- THIS FUNCTION DOESN'T WORK IF IT PUTS TOO MANY EVENTS ON THE STACK
-- function do_scroll(dx, dy)
-- 	local dx = math.floor(dx)
-- 	local dy = math.floor(dy)
-- 	print ("scrolling", dx, dy)
-- 	local ysign = dy < 0 and -1 or 1
-- 	local xsign = dx < 0 and -1 or 1
-- 	local adx = math.abs(dx)
-- 	local ady = math.abs(dy)
-- 	local remainingx = adx
-- 	local remainingy = ady
-- 	local ddx
-- 	local ddy
-- 	local step = 0
-- 	local max_steps = 20
--
-- 	while remainingx > 0 or remainingy > 0 do
-- 		-- do easing first
-- 		ddx = 0
-- 		ddy = 0
-- 		if remainingx > 0 then ddx = 1 end
-- 		if remainingx > 10 then ddx = math.floor(remainingx / 10) end
--
-- 		if remainingy > 0 then ddy = 1 end
-- 		if remainingy > 10 then ddy = math.floor(remainingy / 10) end
--
-- 		print(step, ddx, ddy, remainingx, remainingy)
-- 		remainingx = remainingx - ddx
-- 		remainingy = remainingy - ddy
-- 		if ddx <= 0 and ddy <= 0 then break end
-- 		hs.eventtap.event.newScrollEvent({ddx * xsign, ddy * ysign},{},'pixel'):post()
-- 	end
-- 	print "done"
-- end

mousetap = hs.eventtap.new({5}, function(e)
	oldmousepos = hs.mouse.getAbsolutePosition()
	local mods = hs.eventtap.checkKeyboardModifiers()
	if mods['ctrl'] and mods['cmd'] then
		-- print ("will scroll")
		local dx = e:getProperty(hs.eventtap.event.properties['mouseEventDeltaX'])
		local dy = e:getProperty(hs.eventtap.event.properties['mouseEventDeltaY'])
		local scroll = hs.eventtap.event.newScrollEvent({dx * scrollmult, dy * scrollmult},{},'pixel')
		scroll:post()
		-- do_scroll(dx * scrollmult, dy * scrollmult)
		
		-- put the mouse back
		hs.mouse.setAbsolutePosition(oldmousepos)
		
		-- return true, {scroll}
		return true
	else
		return false, {}
	end
	-- print ("Mouse moved!")
	-- print (dx)
	-- print (dy)
end)
mousetap:start()
