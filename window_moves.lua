-- HANDLE WINDOW MOVING
-- winmovecheck = hs.eventtap.new({10}, function(e)
-- 	print 'hello'
-- 	local inc = 1
-- 	local mods = hs.eventtap.checkKeyboardModifiers()
-- 	if (mods['shift']) then inc = 10 end
--
-- 	local keyname = hs.keycodes.map[e.getKeyCode()]
-- 	print (keyname)
-- 	local delta
-- 	if keyname == 'escape' then winmovecheck:stop() end
-- 	if keyname == 'left' then delta = {x=-inc, y=0} end
-- 	if keyname == 'right' then delta = {x=inc, y=0} end
-- 	if keyname == 'up' then delta = {x=0, y=-inc} end
-- 	if keyname == 'down' then delta = {x=0, y=inc} end
-- 	if (delta) then activewin:move(delta) end
-- 	return true
-- end)

local screens = hs.screen.allScreens()
local keymap = {}
keymap[123] = 'left'
keymap[124] = 'right'
keymap[125] = 'down'
keymap[126] = 'up'
keymap[49]  = 'space'
keymap[53]  = 'escape'
for i = 1, 10 do
	keymap[i+17] = i
end

local isMaximized = false
local oldSize
local oldTopLeft

local oldSize2
local oldTopLeft2


local activewin    = hs.window.orderedWindows()[1]

local winkeywatcher2 = hs.eventtap.new({hs.eventtap.event.types.keyDown}, function(e)
	local code = e:getKeyCode()
	if keymap[code] ~= nil then
		local command = keymap[code]
		local flags = e:getFlags()
		if flags.cmd and flags.ctrl and flags.alt then
			local key=tonumber(e:getCharacters(true))
			if key ~= nil then
				activewin = hs.window.orderedWindows()[1]
				if activewin:screen() == screens[1] then
					oldSize2 = activewin:size()
					oldTopLeft2 = activewin:topLeft()
				end
				activewin:moveToScreen(screens[key], true, true)
				if key == 1 then
					activewin:setSize(oldSize2)
					activewin:setTopLeft(oldTopLeft2)
					isMaximized = false
				else
					activewin:maximize(0)
					isMaximized = true
				end
			end
		end
	end
end):start()

local winkeywatcher
winkeywatcher = hs.eventtap.new({hs.eventtap.event.types.keyDown}, function(e)
	local code = e:getKeyCode()
	if keymap[code] ~= nil then
		local command = keymap[code]
		local flags = e:getFlags()
		local inc = 1
		
		if flags.shift then
			inc = 30
		end
		
		if command == 'left' then
			activewin:move({x=-inc,y=0 })
		elseif command == 'right' then
			activewin:move({x=inc,y=0 })
		elseif command == 'down' then
			activewin:move({x=0,y=inc })
		elseif command == 'up' then
			activewin:move({x=0,y=-inc })
		elseif command == 'space' then
			if isMaximized then
				activewin:setSize(oldSize)
				activewin:setTopLeft(oldTopLeft)
				isMaximized = false
			else
				oldSize = activewin:size()
				oldTopLeft = activewin:topLeft()
				activewin:maximize(0)
				isMaximized = true
			end
		elseif command == 'escape' then
			alert('Window Moving is Disabled')
			winkeywatcher:stop()
		else
			local key=tonumber(e:getCharacters(true))
			if key ~= nil then
				activewin:moveToScreen(screens[key], true, true)
			end
		end
		return true
	end
	return true
end)

winmovestart = hs.hotkey.new({'cmd','ctrl','alt'}, 'm', function()
	screens = hs.screen.allScreens()
	
	alert('Window Moving is Enabled\nARROW KEYS: move\n(use SHIFT for larger moves)\n\nSPACE: maximizes\n\nNUMBER KEYS: change display\n\nESC: end')
	
	-- get topmost window
	activewin = hs.window.orderedWindows()[1]
	
	-- start the keyboard watcher
	winkeywatcher:start()
end)

-- TODO: on cmd ctrl alt number key, move window to that display and maximize it.


winmovestart:enable()